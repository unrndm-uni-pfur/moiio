from pathlib import Path
from subprocess import call
import argparse

from . import __version__

src_path = Path(__file__) / ".." / ".."
src_path = src_path.resolve()

source_path = src_path / ".."
source_path = source_path.resolve()

pyproject_path = source_path / "pyproject.toml"
dist_folder = source_path / "dist"

lab01_path = src_path / "lab01"
lab01_main_path = lab01_path / "__main__.py"

lab02_path = src_path / "lab02"
lab02_main_path = lab02_path / "__main__.py"

class Scripts:
    @staticmethod
    def fix_black():
        call(
            f"python -m black {src_path} --config {pyproject_path}".split(" ")
        )
        call(
            f"python -m black {lab01_path} --config {pyproject_path}".split(" ")
        )

    @staticmethod
    def fix_isort():
        call(f"python -m isort {src_path}".split(" "))
        call(f"python -m isort {lab01_path}".split(" "))

    @staticmethod
    def fix():
        Scripts.fix_black()
        Scripts.fix_isort()

    @staticmethod
    def run_lab01():
        call(f"python {lab01_path}".split(" "))

    @staticmethod
    def build_lab01():
        call(f"cxfreeze {lab01_main_path} --target-dir dist/lab01 --target-name lab01".split())

    @staticmethod
    def run_lab02():
        call(f"python {lab02_path}".split(" "))

    @staticmethod
    def build_lab02():
        call(f"cxfreeze {lab02_main_path} --target-dir dist/lab02 --target-name lab02".split())

    @staticmethod
    def clean():
        call(f"rm -rf {dist_folder}".split())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="utility tool for running ")
    parser.add_argument("lab_name", type=str, help="")

    args = parser.parse_args()

    if args.lab_name in [1, "lab01"]:
        Scripts.run_lab01()
    elif args.lab_name in [2, "lab02"]:
        Scripts.run_lab02()
