import math
import time
from collections import defaultdict
from typing import Any, Dict, List, Optional
from pprint import pp

from PySide2.QtWidgets import QHBoxLayout, QLabel, QLineEdit, QPushButton, QVBoxLayout, QWidget
from PySide2.QtCore import Slot, Qt
from sympy import diff, polys, Function, Symbol, solveset, Eq, S
from sympy.utilities.lambdify import lambdify
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import cm


class Lab01(QWidget):
    def __init__(self):
        super().__init__()

        self.func: Optional[polys.Poly] = None
        self.x0: Optional[np.array] = None
        self.eps: Optional[float] = None
        # self.t0: Optional[float] = None
        self.succ: Optional[bool] = None

        self.step = 0

        self.answer: List[Any] = []


        self.setWindowTitle("Lab 01, Variant 18")

        self.master_layout = QVBoxLayout()

        self.variant_desc = QLabel("Вариант 18, Метод Гаусса-Зейделя.")
        self.variant_desc.setAlignment(Qt.AlignCenter)
        self.master_layout.addWidget(self.variant_desc)

        # function input
        self.func_input_layout = QHBoxLayout()

        self.func_input_description = QLabel("Input function f:")
        self.func_input_layout.addWidget(self.func_input_description)

        self.func_input = QLineEdit("4*(x_1 - 5)^2 + (x_2 - 6)^2")
        self.func_input_layout.addWidget(self.func_input)

        self.master_layout.addLayout(self.func_input_layout)

        # x0 input
        self.x0_input_layout = QHBoxLayout()

        self.x0_input_description = QLabel("Input x\u2080:")
        self.x0_input_layout.addWidget(self.x0_input_description)

        self.x0_input = QLineEdit("8; 9")
        self.x0_input_layout.addWidget(self.x0_input)

        self.master_layout.addLayout(self.x0_input_layout)

        # epsilon input
        self.eps_input_layout = QHBoxLayout()

        self.eps_input_description = QLabel("Input epsilon:")
        self.eps_input_layout.addWidget(self.eps_input_description)

        self.eps_input = QLineEdit("0.1")
        self.eps_input_layout.addWidget(self.eps_input)

        self.master_layout.addLayout(self.eps_input_layout)

        # # t0 input
        # self.t0_input_layout = QHBoxLayout()
        #
        # self.t0_input_description = QLabel("Input t\u2080:")
        # self.t0_input_layout.addWidget(self.t0_input_description)
        #
        # self.t0_input = QLineEdit("0,5")
        # self.t0_input_layout.addWidget(self.t0_input)

        # self.master_layout.addLayout(self.t0_input_layout)

        self.second_step_layout = QVBoxLayout()

        # confirm button
        self.confirm_input = QPushButton("Parse expressions")
        self.confirm_input.clicked.connect(self.parse_input)
        self.second_step_layout.addWidget(self.confirm_input)

        # interpreted function
        self.res_label = QLabel("Result: ")
        self.second_step_layout.addWidget(self.res_label)

        # current step
        self.current_step = QLabel(f"current step: {self.step}")
        self.second_step_layout.addWidget(self.current_step)

        # controls
        self.controls_layout = QHBoxLayout()

        self.minus_step = QPushButton("\u23EA (-1)")
        self.minus_step.clicked.connect(self.step_backward)
        self.controls_layout.addWidget(self.minus_step)

        self.first_step = QPushButton("\u23F9 (stop)")
        self.first_step.clicked.connect(self.step_zero)
        self.controls_layout.addWidget(self.first_step)

        self.compute_step = QPushButton("\u25b6 (compute)")
        self.compute_step.clicked.connect(self.compute_to_end)
        # self.controls_layout.addWidget(self.compute_step)

        self.plus_step = QPushButton("\u23EF (+1)")
        self.plus_step.clicked.connect(self.step_forward)
        self.controls_layout.addWidget(self.plus_step)

        self.second_step_layout.addLayout(self.controls_layout)

        # create empty graph
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.graph = self.figure.add_subplot(111, projection="3d")
        self.graph.set_title("graph", fontdict={"weight": "bold", "color": "#FFC0CB", "size": 18})
        self.graph.set_xlabel("x")
        self.graph.set_ylabel("y")
        self.graph.set_zlabel("z")
        self.second_step_layout.addWidget(self.canvas)
        self.second_step_layout.addWidget(self.toolbar)

        self.master_layout.addLayout(self.second_step_layout)

        self.setLayout(self.master_layout)

    @Slot()
    def parse_input(self):
        try:
            self.func = polys.polytools.poly_from_expr(self.func_input.text().replace(",", "."))[0]
        except Exception as e:
            # self.func_desc.setText(f"Error: {e}")
            pass
        else:
            # self.func_desc.setText("Interpreted function: " + str(self.func))
            print("Interpreted function: " + str(self.func))

        try:
            self.x0 = np.array([float(x0_i.strip()) for x0_i in self.x0_input.text().replace(",", ".").split(";")])
        except Exception as e:
            print(e)
        else:
            print(f"x0 = {self.x0}")

        # try:
        #     self.t0 = float(self.t0_input.text().replace(",", "."))
        # except Exception as e:
        #     print(e)
        # else:
        #     print(f"t0 = {self.t0}")

        try:
            self.eps = float(self.eps_input.text().replace(",", "."))
        except Exception as e:
            print(e)
        else:
            print(f"eps = {self.eps}")

        self.compute()
        if self.succ:
            self.res_label.setText("Result: found min")
            self.draw()

    def compute(self):
        n = len(self.x0)
        grad = [diff(self.func, gen) for gen in self.func.gens]

        x = defaultdict(dict)
        x[0][0] = np.array(self.x0)

        t_k = Symbol("t_k", real=True)
        # t = dict()
        # t[0] = self.t0

        j = 0
        while True:
            for k in range(0, n):
                print(f"{j=}, {k=}")
                pp(x)
                self.step += 1

                # norm = np.sqrt(np.sum([float(grad_i(x[j][k][0], x[j][k][1])) ** 2 for grad_i in grad]))
                # print(f"{norm=}")
                # if norm < self.eps:
                #     break
                print(self.e_i(j+1))
                x[j][k+1] = x[j][k] - (
                        (
                            t_k*(grad[k](x[j][k][0], x[j][k][1]))
                        )*self.e_i(k+1)
                )
                print(f"{x[j][k+1]=}")
                fun_tk = self.func(x[j][k+1][0], x[j][k+1][1])
                print(f"{fun_tk=}")
                fun_tk_diff = diff(fun_tk, t_k)
                print(f"{fun_tk_diff=}")

                tk_value = list(solveset(Eq(fun_tk_diff, 0), t_k))
                if len(tk_value) == 1:
                    tk_value = tk_value[0]
                else:
                    print(tk_value)
                    raise ValueError("Multiple solutions for t")
                print(f"{tk_value=}")
                x[j][k+1] = np.array(lambdify(t_k, x[j][k+1], "numpy")(tk_value), dtype=np.float)
                print(f"{x[j][k+1]=}")
                print("-"*10)

                # old_val = self.func(x[j][k][0], x[j][k][1])
                # new_val = self.func(x[j][k+1][0], x[j][k+1][1])
                # if new_val >= old_val:
                #     raise print("doesn't minimize")
                # print(f"{old_val=}, {new_val=}")

            pp(x)
            norm = np.sqrt(np.sum([float(g(*x[j][n]))**2 for g in grad]))
            print(f"{norm=}")
            if norm >= self.eps:
                x[j + 1][0] = x[j][n]
                j += 1
            else:
                break
        x_answer = x[j][n]
        print(f"{x_answer=}")
        self.answer = x

        hessian = np.array([[diff(grd, gen).expr for grd in grad] for gen in self.func.gens], dtype=np.float)
        print(f"{hessian=}")

        delta = np.array([np.linalg.det(hessian[0:i, 0:i])for i in range(1, n + 1)])
        print(f"{delta=}")

        point = ""
        if all(delta > 0):
            point = "min"
        else:
            current = True
            for d in delta:
                if (d > 0) == current:
                    current = not current
                else:
                    point = "none"
                    break
                point = "max"
        print(point)
        if point == "max":
            print("found MAX")
        elif point == "min":
            print("found MIN")
            self.succ = True
        else:
            print("found nothing")

    @Slot()
    def step_forward(self):
        self.step += 1
        self.display_step()

    @Slot()
    def step_backward(self):
        self.step = max(0, self.step - 1)
        self.display_step()

    @Slot()
    def step_zero(self):
        self.step = 0
        self.display_step()

    @Slot()
    def compute_to_end(self):
        max_step = sum([ len(v1) for _, v1 in self.answer.items()])
        for i in range(max_step):
            self.step = 0
            self.display_step()

    def e_i(self, i: int) -> np.array:
        return np.eye(1, len(self.func.gens), i-1)[0]

    def display_step(self):
        self.current_step.setText(f"current step: {self.step}")
        self.graph.clear()
        self.graph.set_title("graph", fontdict={"weight": "bold", "color": "#FFC0CB", "size": 18})
        self.graph.set_xlabel("x")
        self.graph.set_ylabel("y")
        self.graph.set_zlabel("z")

        xa, ya, za = [], [], []
        to_break = False
        for k1, v1 in self.answer.items():
            for k2, x_a in v1.items():
                xa.append(x_a[0])
                ya.append(x_a[1])
                za.append(self.func(x_a[0], x_a[1]))
                if len(xa) >= self.step:
                    to_break=True
                if to_break:
                    break
            if to_break:
                break

        self.graph.plot(xa, ya, za, color="black", label="solutions")

        X = np.arange(-10, 10, 0.5)
        Y = np.arange(-10, 10, 0.5)
        X, Y = np.meshgrid(X, Y)
        z = [[self.func(x_l, y_l) for x_l, y_l in zip(x_u, y_u)] for x_u, y_u in zip(X, Y)]
        Z = np.array(z, dtype=np.float)

        surf = self.graph.plot_surface(
            X,
            Y,
            Z,
            cmap=cm.rainbow,
            linewidth=0,
            antialiased=False,
            alpha=0.5,
            label="function surface"
        )
        surf._facecolors2d = surf._facecolors3d
        surf._edgecolors2d = surf._edgecolors3d

        self.graph.legend()
        self.canvas.draw()

    def draw(self):
        self.current_step.setText(f"current step: {self.step}")
        self.graph.clear()
        self.graph.set_title("graph", fontdict={"weight": "bold", "color": "#FFC0CB", "size": 18})
        self.graph.set_xlabel("x")
        self.graph.set_ylabel("y")
        self.graph.set_zlabel("z")

        xa, ya, za = [], [], []
        for k1, v1 in self.answer.items():
            for k2, x_a in v1.items():
                xa.append(x_a[0])
                ya.append(x_a[1])
                za.append(self.func(x_a[0], x_a[1]))

        self.graph.plot(xa, ya, za, color="black", label="solutions")


        X = np.arange(-10, 10, 0.5)
        Y = np.arange(-10, 10, 0.5)
        X, Y = np.meshgrid(X, Y)
        z = [[ self.func(x_l, y_l) for x_l, y_l in zip(x_u, y_u)] for x_u, y_u in zip(X, Y) ]
        Z = np.array(z, dtype=np.float)

        surf = self.graph.plot_surface(
            X,
            Y,
            Z,
            cmap=cm.rainbow,
            linewidth=0,
            antialiased=False,
            alpha=0.5,
            label="function surface"
        )
        surf._facecolors2d = surf._facecolors3d
        surf._edgecolors2d = surf._edgecolors3d

        self.graph.legend()
        self.canvas.draw()
