import sys

from PySide2.QtWidgets import QApplication, QLabel

from lab01 import Lab01

if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = Lab01()
    widget.resize(800, 600)
    widget.show()

    sys.exit(app.exec_())
