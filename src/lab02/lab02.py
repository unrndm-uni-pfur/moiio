import random
from PySide2.QtWidgets import QLabel, QPushButton, QVBoxLayout, QWidget
from PySide2.QtCore import Slot, Qt


class MyWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Lab 02")

        self.hello = ["Hallo Welt", "你好，世界", "Hei maailma",
            "Hola Mundo", "Привет мир"]

        self.button = QPushButton("Click me!")
        self.text = QLabel("Hello World - 2")
        self.text.setAlignment(Qt.AlignCenter)

        self.layout = QVBoxLayout()
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)

        # Connecting the signal
        self.button.clicked.connect(self.magic)

    @Slot()
    def magic(self):
        self.text.setText(random.choice(self.hello))