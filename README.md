MOIIO
=====

Development:
------------
```shell script
python3 -m venv .venv
poetry install
briefcase create --no-input
briefcase dev -a lab01 # for lab01
briefcase dev -a lab02 # for lab02
```
